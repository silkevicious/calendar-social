# Calendar.social
# Copyright (C) 2018 Gergely Polonkai
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Security related things for Calendar.social
"""

from flask import current_app, session
from flask_login.signals import user_logged_in, user_logged_out
from flask_security import Security, AnonymousUser as BaseAnonymousUser

security = Security()


class AnonymousUser(BaseAnonymousUser):
    """Anonymous user class for Calendar.social
    """

    @property
    def timezone(self):
        """The time zone of the anonymous user
        """

        return current_app.timezone

    @property
    def profile(self):
        """The profile of the anonymous user

        Always evaluates to ``None``
        """

        return None


@user_logged_in.connect
def login_handler(app, user):  # pylint: disable=unused-argument
    """Signal handler to be called when a user successfully logs in
    """

    from .models import AuditLog

    AuditLog.log(user, AuditLog.TYPE_LOGIN_SUCCESS)

    user.active_sessions += [session.sid]


@user_logged_out.connect
def logout_handler(app, user):  # pylint: disable=unused-argument
    """Signal handler to be called when a user logs out
    """

    from .models import AuditLog

    AuditLog.log(user, AuditLog.TYPE_LOGOUT)
