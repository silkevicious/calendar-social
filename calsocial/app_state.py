# Calendar.social
# Copyright (C) 2018  Gergely Polonkai
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Metaclass for storing and accessing app state
"""

def get_state_base(self, key):
    """Method to get a key from the state store
    """

    return self.__get_state__(key)

def set_state_base(self, key, value):
    """Method to set a key/value in the state store
    """

    self.__set_state__(key, str(value))

def set_default_base(self, key, value):
    """Method to set the default value of a key in the state store

    If key is already in the state store, this method is a no-op.
    """

    self.__set_state_default__(key, str(value))


def app_state_base(klass):
    """Base class creator for AppStateMeta types

    :param klass: the class to extend
    :type klass: type
    :returns: a new class extending ``klass``
    :rtype: type
    """

    # Construct the meta class based on the metaclass of ``klass``
    metaclass = type(
        klass.__name__ + 'BaseMeta',
        (type(klass),),
        {
            '__getitem__': get_state_base,
            '__setitem__': set_state_base,
            'setdefault': set_default_base,
        })

    return metaclass(klass.__name__ + 'Base', (klass,), {'__abstract__': True})
